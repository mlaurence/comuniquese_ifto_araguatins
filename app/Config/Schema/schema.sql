SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';


-- -----------------------------------------------------
-- Table `contatos`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `contatos` ;

CREATE TABLE IF NOT EXISTS `contatos` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(255) NOT NULL,
  `email` VARCHAR(255) NOT NULL,
  `telefone` VARCHAR(255) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mensagens`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mensagens` ;

CREATE TABLE IF NOT EXISTS `mensagens` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `oque` VARCHAR(255) NOT NULL,
  `quem` VARCHAR(255) NOT NULL,
  `quando` VARCHAR(255) NOT NULL,
  `onde` VARCHAR(255) NOT NULL,
  `como` VARCHAR(255) NOT NULL,
  `porque` VARCHAR(255) NOT NULL,
  `outras_informacoes` TEXT NULL,
  `nome` VARCHAR(255) NOT NULL,
  `email` VARCHAR(255) NOT NULL,
  `telefone` VARCHAR(255) NULL,
  `created` DATETIME NULL,
  `status` TINYINT NOT NULL DEFAULT 0,
  `contato_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_mensagens_destinos1`
    FOREIGN KEY (`contato_id`)
    REFERENCES `contatos` (`id`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT)
ENGINE = InnoDB;

CREATE INDEX `fk_mensagens_destinos1_idx` ON `mensagens` (`contato_id` ASC);


-- -----------------------------------------------------
-- Table `anexos`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `anexos` ;

CREATE TABLE IF NOT EXISTS `anexos` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `arquivo` VARCHAR(255) NOT NULL,
  `nome` VARCHAR(255) NOT NULL,
  `mimetype` VARCHAR(255) NOT NULL,
  `mensagem_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_anexos_mensagens1`
    FOREIGN KEY (`mensagem_id`)
    REFERENCES `mensagens` (`id`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT)
ENGINE = InnoDB;

CREATE INDEX `fk_anexos_mensagens1_idx` ON `anexos` (`mensagem_id` ASC);


-- -----------------------------------------------------
-- Table `users`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `users` ;

CREATE TABLE IF NOT EXISTS `users` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `username` VARCHAR(255) NOT NULL,
  `password` VARCHAR(255) NOT NULL,
  `role` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
