<?php
class ContatosController extends AdminAppController
{
    public $name = 'Contatos';
    public $uses = array('Contato');

    public $components = array('Paginator');
    public $paginate = array(
        'Contato' => array(
            'limit' => 10,
            'order' => array(
                'Contato.id' => 'asc',
            ),
        ),
    );

    public function index()
    {
        $this->Paginator->settings = $this->paginate;
        $this->set('contatos', $this->Paginator->paginate('Contato'));
    }

    public function adicionar()
    {
        if ($this->request->is('post')) {
            if ($this->Contato->saveAll($this->request->data)) {
                $this->Session->setFlash('Novo contato adicionado com sucesso.', 'success');
                $this->redirect(array('action' => 'editar', $this->Contato->id));
            } else {
                $this->Session->setFlash('Ops, algo deu errado. Por favor, verifique os dados e tente novamente.', 'error');
            }
        }
    }

    public function editar($id = null)
    {
        if ($this->request->is('put')) {
             if ($this->Contato->saveAll($this->request->data)) {
                $this->Session->setFlash('Contato modificado com sucesso.', 'success');
            } else {
                $this->Session->setFlash('Ops, algo deu errado. Por favor, verifique os dados e tente novamente.', 'error');
            }
        } else {
            if ($id) {
                $this->request->data = $this->Contato->findById($id);
            } else {
                $this->redirect('index');
            }
        }
    }

    public function deletar($id = null)
    {
        if ($id) {
            if ($this->Contato->delete($id)) {
                $this->Session->setFlash('Contato removido com sucesso.', 'success');
            } else {
                $this->Session->setFlash('Ops, algo deu errado. Por favor, tente novamente.', 'error');
            }
        }
        $this->redirect('index');
    }
}
