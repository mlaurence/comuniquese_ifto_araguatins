<?php
class AdminAppController extends AppController
{
    public $components = array(
        'Session',
        'Auth' => array(
            'loginAction' => array(
                'controller' => 'users',
                'action' => 'login',
            ),
            'authError' => 'Você não tem permissão para acessar este recurso.',
            'authenticate' => array(
                'Form',
            ),
            'authorize' => array('Controller'),
        ),
    );

    public $helpers = array(
        'Html',
        'Form' => array(
            'className' => 'Admin.AdminBootstrapForm',
        ),
    );


    public function isAuthorized($user)
    {
        if (isset($user) && isset($user['role'])) {
            return true;
        }
        return false;
    }
}
