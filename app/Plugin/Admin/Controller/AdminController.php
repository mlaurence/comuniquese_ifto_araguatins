<?php
class AdminController extends AdminAppController
{
    public $name = 'Admin';
    public $uses = array('User', 'Contato', 'Mensagem');

    public function index()
    {
        if ($this->Auth->user('role') == 'root') {
            $this->set('num_usuarios', $this->User->find('count'));
        }
        $this->set('num_contatos', $this->Contato->find('count'));
        $this->set('num_mensagens', $this->Mensagem->find('count'));
        $this->set('num_mensagens_nao_enviadas', $this->Mensagem->find('count', array('conditions' => array('status' => Mensagem::STATUS_NAO_ENVIADA))));
    }
}
