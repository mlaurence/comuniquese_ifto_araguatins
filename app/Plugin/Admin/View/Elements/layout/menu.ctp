<nav class="navbar navbar-default" role="navigation" id="menu">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
            <span class="sr-only">Alternar navega&ccedil;&atilde;o</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <!-- <a class="navbar-brand" href="/admin"><span class="glyphicon glyphicon-home"></span>&nbsp;</a> -->
    </div>
    
    <?php if ($this->Session->read('Auth.User')): ?>
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li <?php echo ($this->params['controller'] == 'admin') ? 'class="active"' : '' ?>><a href="/admin"><span class="glyphicon glyphicon-home"></span>&nbsp;In&iacute;cio</a></li>
                <li <?php echo ($this->params['controller'] == 'mensagens') ? 'class="active"' : '' ?>><a href="/admin/mensagens"><span class="glyphicon glyphicon-inbox"></span>&nbsp;Mensagens</a></li>
                <li <?php echo ($this->params['controller'] == 'contatos') ? 'class="active"' : '' ?>><a href="/admin/contatos"><span class="glyphicon glyphicon-envelope"></span>&nbsp;Contatos</a></li>
                <?php if ($this->Session->read('Auth.User.role') == 'root'): ?>
                    <li <?php echo ($this->params['controller'] == 'users') ? 'class="active"' : '' ?>><a href="/admin/users"><span class="glyphicon glyphicon-user"></span>&nbsp;Usu&aacute;rios</a></li>
                <?php endif; ?>
            </ul>

            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">&nbsp;<?php echo $this->Session->read('Auth.User.username'); ?>&nbsp;<b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="/admin/users/changepassword/"><span class="glyphicon glyphicon-cog"></span>&nbsp;Alterar Senha</a></li>
                        <li class="divider"></li>
                        <li><a href="/admin/users/logout/"><span class="glyphicon glyphicon-log-out"></span>&nbsp;Sair</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    <?php endif; ?>
</nav>
