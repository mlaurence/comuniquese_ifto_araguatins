<hr/>
<div class="row">
    <div class="col-xs-12">
        <div class="center-block">
            <small>
            <?php
                echo $this->Paginator->counter(
                    'Mostrando {:current} registros de {:count} no total.'
                );
            ?>
            </small>
        </div>
    </div>
</div>
<?php if ($this->Paginator->hasPage(2)): ?>
<div class="row">
    <div class="col-xs-12">
        <div class="center-block">
            <ul class="pagination">
            <?php
                $first = $this->Html->tag('span', '', array('class' => 'glyphicon glyphicon-fast-backward'));
                $last = $this->Html->tag('span', '', array('class' => 'glyphicon glyphicon-fast-forward'));
                echo $this->Paginator->first('&laquo;', array('tag' => 'li', 'title' => 'Primeira página', 'escapeTitle' => false));
                echo $this->Paginator->numbers(array('tag' => 'li', 'separator' => false, 'currentClass' => 'active', 'currentTag' => 'span'));
                echo $this->Paginator->last('&raquo;', array('tag' => 'li', 'title' => 'Última página', 'escapeTitle' => false));
            ?>
            </ul>
        </div>
    </div>
</div>
<?php endif; ?>
