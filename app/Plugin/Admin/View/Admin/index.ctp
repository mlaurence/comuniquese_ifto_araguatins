<div class="row">
    <div class="col-xs-12">
        <div class="jumbotron">
            <h2>Bem-vindo!</h2>
            <p>Abaixo você encontra um resumo das informações contidas no sistema.</p>
            <ul class="list-group">
            <?php if ($num_mensagens > 0): ?>
                <li class="list-group-item">
                <?php
                    if ($num_mensagens_nao_enviadas == 0):
                ?>
                        Nenhuma mensagem não enviada
                <?php
                    else:
                ?>
                        <?php echo $num_mensagens_nao_enviadas; ?> mensagens n&atilde;o enviadas
                <?php
                    endif;
                ?>
                </li>
            <?php endif; ?>
                <li class="list-group-item">
                <?php
                    if ($num_mensagens == 0):
                ?>
                        Nenhuma mensagem
                <?php
                    else:
                ?>
                        <?php echo $num_mensagens; ?> mensagens no total
                <?php
                    endif;
                ?>
                </li>
                <li class="list-group-item">
                    <?php echo ($num_contatos == 1 ? $num_contatos . ' contato' : $num_contatos . ' contatos'); ?>
                </li>
            <?php
                if (isset($num_usuarios)):
            ?>
                    <li class="list-group-item"><?php echo ($num_usuarios == 1 ? $num_usuarios . ' usu&aacute;rio' : $num_usuarios . ' usu&aacute;rios'); ?></li>
            <?php
                endif;
            ?>
            </ul>
        </div>
    </div>
</div>
