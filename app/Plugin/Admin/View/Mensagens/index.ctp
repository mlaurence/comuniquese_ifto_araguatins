<div class="row">
    <div class="col-xs-12">
        <h2>Mensagens</h2>
    </div>
</div>
<div class="row">
    <div class="col-xs-12">
        <div class="table-responsive">
            <table class="table table-condensed">
                <thead>
                    <tr>
                        <th>De</th>
                        <th>Para</th>
                        <th>Cadastrada em</th>
                        <th>Status</th>
                        <th>A&ccedil;&otilde;es</th>
                    </tr>
                </thead>
                <tbody>
                <?php
                    foreach ($mensagens as $key => $mensagem):
                ?>
                        <tr <?php echo ($mensagem['Mensagem']['status'] == Mensagem::STATUS_NAO_ENVIADA) ? 'class="warning"' : 'class="success"'; ?>>
                            <td><?php echo $mensagem['Mensagem']['nome']; ?></td>
                            <td><?php echo $mensagem['Contato']['nome']; ?></td>
                            <td><?php echo $this->Time->format('d/m/Y H:i', $mensagem['Mensagem']['created']); ?></td>
                            <td><?php echo ($mensagem['Mensagem']['status'] == Mensagem::STATUS_ENVIADA) ? 'Enviada' : 'Não Enviada'; ?></td>
                            <td>
                                <?php echo $this->Html->link($this->Html->tag('span', '', array('class' => 'glyphicon glyphicon-eye-open')), array('action' => 'ver', $mensagem['Mensagem']['id']), array('class' => 'btn btn-default', 'title' => 'Visualizar', 'escapeTitle' => false)); ?>
                                <?php
                                    if ($mensagem['Mensagem']['status'] == Mensagem::STATUS_NAO_ENVIADA) {
                                        echo $this->Html->link($this->Html->tag('span', '', array('class' => 'glyphicon glyphicon-refresh')), array('action' => 'reenviar', $mensagem['Mensagem']['id']), array('class' => 'btn btn-default', 'title' => 'Reenviar', 'escapeTitle' => false));
                                    } else {
                                        echo $this->Html->link($this->Html->tag('span', '', array('class' => 'glyphicon glyphicon-remove')), array('action' => 'deletar', $mensagem['Mensagem']['id']), array('class' => 'btn btn-default', 'title' => 'Deletar', 'escapeTitle' => false), 'Tem certeza que deseja deletar a mensagem de '.$mensagem['Mensagem']['nome'].' enviada em '.$this->Time->format('d/m/Y H:i', $mensagem['Mensagem']['created']).'?');
                                    }
                                ?>
                            </td>
                <?php
                    endforeach;
                ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<?php echo $this->element('layout/paginacao'); ?>
