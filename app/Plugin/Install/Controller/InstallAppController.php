<?php
App::uses('Controller', 'Controller');

class InstallAppController extends Controller
{
    public function beforeFilter()
    {
        if (Configure::read('Database.installed') == 'true') {
            $this->redirect(array('plugin' => 'admin', 'controller' => 'admin'));
        }

        parent::beforeFilter();
    }
}
