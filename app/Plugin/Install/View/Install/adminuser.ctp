<?php echo $this->element('progress', array('porcentagem' => '50')); ?>
<div class="row">
    <div class="col-xs-12">
    <?php
        echo $this->Form->create('User');
            echo $this->Form->input('User.username', array('type' => 'text', 'label' => 'Nome do Usuário'));
            echo $this->Form->input('User.new_password1', array('type' => 'password', 'label' => 'Senha'));
            echo $this->Form->input('User.new_password2', array('type' => 'password', 'label' => 'Repita a Senha'));
        echo $this->Form->end('Criar Administrador');
    ?>
    </div>
</div>
