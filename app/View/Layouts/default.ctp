<!DOCTYPE html>
<html lang="pt-br">
<head>
    <?php echo $this->Html->charset(); ?>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="robot" content="index, follow" />
    <meta name="description" content="Site do projeto Comunique-se." />
    <meta name="keywords" content="comunique-se,comunicação,contato,informações" />
    <meta name="author" content="Ricardo Moro" />
    <title>Comunique-se!</title>
    <?php
        //META
        echo $this->Html->meta('icon');
        echo $this->fetch('meta');

        //CSS
        echo $this->Html->css('bootstrap.min');
        echo $this->Html->css('barra-governo');
        echo $this->Html->css('style');
        echo $this->fetch('css');

        //SCRIPT
        echo '<!--[if lt IE 9]>';
            echo $this->Html->script('html5shiv');
            echo $this->Html->script('respond.min');
            echo $this->Html->script('respond.matchmedia.addListener.min');
        echo '<![endif]-->';
        echo $this->Html->script('jquery-1.11.0.min');
        echo $this->Html->script('jquery-migrate-1.2.1.min');
        echo $this->Html->script('bootstrap.min');
        echo $this->Html->script('random-bg');
        echo $this->fetch('script');
    ?>
</head>
<body>
    <!-- Barra do Governo -->
    <?php echo $this->element('layout/barra_governo'); ?>
    
    <!-- Cabeçalho -->
    <header class="row">
        <div class="col-xs-12">
            <img src="/img/comuniquese.png" alt="Logotipo do sítio Comunique-se!" class="img-responsive center-block" />
            <h1 class="sr-only"><?php echo Configure::read('App.shortname'); ?></h1>
        </div>
    </header>

    <!-- Menu -->
    <section class="container">
        <div class="row">
            <div class="col-xs-12">
                <?php echo $this->element('layout/menu'); ?>
            </div>
        </div>
    </section>

    <!-- Conteúdo -->
    <?php echo $this->fetch('page-header'); ?>

    <?php if ($this->Session->check('Message.flash')): ?>
            <!-- Flash Message -->
            <div class="row" id="message">
                <div class="col-xs-12">
                    <?php echo $this->Session->flash(); ?>
                </div>
            </div>
    <?php endif; ?>
    
    <section class="container" id="content">
        <a href="#inicioConteudo" id="inicioConteudo" class="sr-only" accesskey="2">In&iacute;cio do conte&uacute;do</a>
        <?php echo $this->fetch('content'); ?>
        <a href="#" class="sr-only">Fim do conte&uacute;do</a>
    </section>
    
    <!-- Rodapé -->
    <footer class="row">
        <div class="panel panel-default">
            <div class="panel-body col-xs-12">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-sm-push-6">
                        <div class="info">
                            <p><strong><?php echo Configure::read('App.longname'); ?></strong></p>
                            <br/>
                            <p id="desenvolvimento">
                                <em>Desenvolvimento</em>
                                <br/>
                                <a href="http://www.ifrs.edu.br/"><strong>Instituto Federal do Rio Grande do Sul</strong></a>
                            </p>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-5 col-sm-pull-5 col-sm-offset-1">
                        <img src="/img/logo.png" alt="Logotipo do IFRS" class="img-responsive" />
                    </div>
                </div>
            </div>
            <div class="panel-footer col-xs-12">
                <a href="https://bitbucket.org/ricardomoro/comuniquese/">C&oacute;digo-fonte deste s&iacute;tio sob a licen&ccedil;a GPLv3</a>
            </div>
        </div>
    </footer>
    <?php echo $this->element('sql_dump'); ?>
    <!-- Ajax Loader -->
    <div id="loader">
        <img src="/img/loader.gif" alt="Carregando..." class="ajax-loader"/>
    </div>
</body>
</html>
