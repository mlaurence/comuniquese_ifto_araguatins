<?php
    $title = 'Contatos';
    $text = array();
    $text[] = 'Abaixo voc&ecirc; encontra as formas de contato com os comunicadores de todas as unidades.';
    $this->start('page-header');
        echo $this->element('layout/page-header', array('title' => $title, 'text' => $text));
    $this->end();
?>
<?php
    $num = count($contatos);
    $limite = ceil($num / 2);
?>
<?php if ($num > 0): ?>
    <div class="row">
        <div class="col-xs-12 col-sm-6">
            <ul class="contato">
            <?php for ($i = 0; $i < $limite; $i++): ?>
                <li>
                    <h3><?php echo $contatos[$i]['Contato']['nome']; ?></h3>
                    <p><a href="mailto:<?php echo $contatos[$i]['Contato']['email']; ?>"><?php echo $contatos[$i]['Contato']['email']; ?></a></p>
                    <p><?php echo $contatos[$i]['Contato']['telefone']; ?></p>
                </li>
            <?php endfor; ?>
            </ul>
        </div>
        <div class="col-xs-12 col-sm-6">
            <ul class="contato">
            <?php for ($i = $limite; $i < $num; $i++): ?>
                <li>
                    <h3><?php echo $contatos[$i]['Contato']['nome']; ?></h3>
                    <p><a href="mailto:<?php echo $contatos[$i]['Contato']['email']; ?>"><?php echo $contatos[$i]['Contato']['email']; ?></a></p>
                    <p><?php echo $contatos[$i]['Contato']['telefone']; ?></p>
                </li>
            <?php endfor; ?>
            </ul>
        </div>
    </div>
<?php else: ?>
    <div class="row">
        <div class="col-xs-12">
            <h3>Ainda não existem contatos cadastrados</h3>
        </div>
    </div>
<?php endif; ?>