<p>Informações encaminhadas pelo site <em>Comunique-se!</em></p>
<br/>
<p>
    <strong>O quê?</strong><br/>
    <?php echo $data['Mensagem']['oque']; ?>
</p>
<p>
    <strong>Quem?</strong><br/>
    <?php echo $data['Mensagem']['quem']; ?>
</p>
<p>
    <strong>Quando?</strong><br/>
    <?php echo $data['Mensagem']['quando']; ?>
</p>
<p>
    <strong>Onde?</strong><br/>
    <?php echo $this->request->data['Mensagem']['onde']; ?>
</p>
<p>
    <strong>Como?</strong><br/>
    <?php echo $data['Mensagem']['como']; ?>
</p>
<p>
    <strong>Por quê?</strong><br/>
    <?php echo $data['Mensagem']['porque']; ?>
</p>
<p>
    <strong>Outras informações:</strong><br/>
    <?php echo $data['Mensagem']['outras_informacoes']; ?>
</p>
<br/>
<p>
    <strong>Remetente</strong><br/>
    Nome: <?php echo $data['Mensagem']['nome']; ?><br/>
    Email: <?php echo $data['Mensagem']['email']; ?><br/>
    Telefone: <?php echo $data['Mensagem']['telefone']; ?>
</p>
