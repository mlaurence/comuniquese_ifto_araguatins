<?php
    $title = 'Subs&iacute;dios';
    $text = array();
    $text[] = 'Abaixo voc&ecirc; encontra alguns livros digitais e manuais de reda&ccedil;&atilde;o, ou fotografia, caso queira saber mais sobre como ajudar o comunicador da sua unidade a trabalhar o material que voc&ecirc; enviar&aacute;.';
    $this->start('page-header');
        echo $this->element('layout/page-header', array('title' => $title, 'text' => $text));
    $this->end();
?>
<div class="row">
    <div class="col-xs-12">
        <h3>Lista de Livros e Manuais</h3>
    </div>
</div>
<div class="row">
	<div class="col-xs-12">
		<!-- <p class="arquivo"><a class="pdf" href="#">Manual de reda&ccedil;&atilde;o para sites do IFRS</a></p>
		<p class="autor">(em produ&ccedil;&atilde;o)</p>
		<p class="desc"></p> -->

		<p class="arquivo"><a class="pdf" href="/files/Como escrever para a web.pdf">Como escrever para a web – Elementos para a discuss&atilde;o e constru&ccedil;&atilde;o de manuais de reda&ccedil;&atilde;o online</a></p>
		<p class="autor">(Guillermo Franco, 2009)</p>
		<p class="desc">Livro de 223 p&aacute;ginas sobre reda&ccedil;&atilde;o jornal&iacute;stica e internet.</p>
		
		<p class="arquivo"><a class="pdf" href="/files/Jornalismo 2.0.pdf">Jornalismo 2.0 – Como sobreviver e prosperar na era da cultura da informa&ccedil;&atilde;o</a></p>
		<p class="autor">(Mark Briggs, 2007)</p>
		<p class="desc">Livro de 134 p&aacute;ginas sobre jornalismo para a internet.</p>
		
		<p class="arquivo"><a class="pdf" href="/files/Manual de redação para a web.pdf">Manual de reda&ccedil;&atilde;o na web</a></p>
		<p class="autor">(Governo do Estado de S&atilde;o Paulo)</p>
		<p class="desc">Apostila de 32 p&aacute;ginas com conceitos de <em>webwriting</em>.</p>
		
		<p class="arquivo"><a class="pdf" href="/files/Gramática do texto jornalístico.pdf">Gram&aacute;tica do texto jornal&iacute;stico</a></p>
		<p class="autor">(S&eacute;rie de 8 aulas dispon&iacute;vel no site do curso de Jornalismo da UFSC, 2004)</p>
		<p class="desc">Texto de 50 p&aacute;ginas.</p>
		
		<p class="arquivo"><a class="pdf" href="/files/Dicas para escrever bem.pdf">Dicas para escrever bem</a></p>
		<p class="autor">(Eduardo da Rocha – Faatesp, 2009)</p>
		<p class="desc">Guia simples e bem humorado de 9 p&aacute;ginas.</p>
		
		<p class="arquivo"><a class="pdf" href="/files/Jornalismo para quem não é jornalista.pdf">Jornalismo para quem n&atilde;o &eacute; jornalista – Internet</a></p>
		<p class="autor">(Marcos Alexandre Oliveira, 2006)</p>
		<p class="desc">Apostila de 36 p&aacute;ginas.</p>
	</div>
</div>
