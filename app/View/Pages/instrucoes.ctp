<?php
    $title = 'Instru&ccedil;&otilde;es';
    $text = array();
    $text[] = 'Leia atentamente as instru&ccedil;&otilde;es abaixo antes de enviar seu material para garantir o sucesso de sua a&ccedil;&atilde;o.';
    $this->start('page-header');
        echo $this->element('layout/page-header', array('title' => $title, 'text' => $text));
    $this->end();
?>
<div class="row">
    <div class="col-xs-12">
        <h3>Leia atentamente as instru&ccedil;&otilde;es</h3>
    </div>
</div>
<div class="row">
	<div class="col-xs-12">
		<p>Preencha todos os campos obrigat&oacute;rios do formul&aacute;rio. Eles s&atilde;o importantes para a montagem do <em>lide</em>, como &eacute; chamado o primeiro par&aacute;grafo de uma mat&eacute;ria jornal&iacute;stica. Um bom <em>lide</em> consegue responder todas ou o m&aacute;ximo das perguntas principais: O qu&ecirc;, quem, quando, como, onde e por qu&ecirc;.</p>
		<p>Envie arquivos com no m&aacute;ximo 1 MB. No caso de fotos, essa qualidade &eacute; suficiente no uso em <em>sites</em>. Caso envie arquivos maiores, o filtro do e-mail poder&aacute; impedir o recebimento.</p>
	</div>
</div>
