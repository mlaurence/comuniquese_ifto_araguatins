<?php
    $title = 'Envie informa&ccedil;&otilde;es';
    $text = array();
    $text[] = 'Utilize o formul&aacute;rio abaixo para enviar as informa&ccedil;&otilde;es m&iacute;nimas para que o comunicador de sua unidade possa montar uma pauta para uma posterior reportagem ou entrevista, ou ainda escrever uma nota sobre sua not&iacute;cia.';
    $text[] = 'Lembre que &eacute; importante que as informa&ccedil;&otilde;es sejam, de prefer&ecirc;ncia, enviadas antes de o evento ocorrer, assim o comunicador pode avaliar a possibilidade de uma cobertura maior.';
    $this->start('page-header');
        echo $this->element('layout/page-header', array('title' => $title, 'text' => $text));
    $this->end();
?>

<div class="row">
    <div class="col-xs-12">
        <?php
            echo $this->Form->create('Mensagem', array('type' => 'file', 'url' => array('controller' => 'mensagens', 'action' => 'enviar')));
                echo $this->element('forms/mensagem');
            echo $this->Form->end(array('label' => 'Enviar', 'class' => 'btn btn-primary pull-right', 'before' => '', 'after' => ''));
        ?>
    </div>
</div>
