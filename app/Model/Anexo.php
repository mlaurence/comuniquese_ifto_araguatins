<?php
// App::uses('File', 'Utility');

class Anexo extends AppModel
{
    public $name = 'Anexo';
    
    public $validate = array(
        'arquivo' => array(
            'regra1' => array(
                'rule' => array('filesize', '<=', '1MB'),
                'message' => 'O arquivo não pode ser maior que 1MB.',
            ),
            'regra2' => array(
                'rule' => 'uploadError',
                'message' => 'Ocorreu um erro com o envio do arquivo. Por favor, tente novamente.'
            ),
        ),
    );

    public function beforeSave($options = array())
    {
        parent::beforeSave($options);
        
        if (isset($this->data['Anexo'])) {
            if (isset($this->data['Anexo']['arquivo'])) {
                if (is_array($this->data['Anexo']['arquivo'])) {
                    $filename = md5($this->data['Anexo']['arquivo']['size'] . date('dmYHis'));
                    $ext = strtolower(pathinfo($this->data['Anexo']['arquivo']['name'], PATHINFO_EXTENSION));
                    $file = $filename . '.' . $ext;
                    if (move_uploaded_file($this->data['Anexo']['arquivo']['tmp_name'], WWW_ROOT . 'files/uploads/' . $file)) {
                        $this->data['Anexo']['nome'] = $this->data['Anexo']['arquivo']['name'];
                        $this->data['Anexo']['mimetype'] = $this->data['Anexo']['arquivo']['type'];
                        $this->data['Anexo']['arquivo'] = $file;
                    } else {
                        $this->data['Anexo']['arquivo'] = NULL;
                        return false;
                    }
                }
            }
        }
        
        return true;
    }

    // public function afterDelete()
    // {
    //     if (!isset($this->data[$this->alias])) {
    //         foreach ($this->data as $key => $value) {
    //             $file = new File(WWW_ROOT.'/files/uploads/'.$this->data[$key][$this->alias]['arquivo']);
    //             $file->delete();
    //         }
    //     } else {
    //         $file = new File(WWW_ROOT.'/files/uploads/'.$this->data[$this->alias]['arquivo']);
    //         $file->delete();
    //     }
    // }
}
