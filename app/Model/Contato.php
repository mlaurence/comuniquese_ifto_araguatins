<?php
class Contato extends AppModel
{
    public $name = 'Contato';

    public $validate = array(
        'nome' => array(
            'regra1' => array(
                'rule' => 'notEmpty',
                'message' => 'O Nome não pode ser deixado em branco.',
            ),
        ),
        'email' => array(
            'regra1' => array(
                'rule' => 'notEmpty',
                'message' => 'O Email não pode ser deixado em branco.',
            ),
            'regra2' => array(
                'rule' => 'email',
                'message' => 'O Email deve ser válido.',
            ),
        ),
    );
}
