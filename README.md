# Comunique-se!

Site e sistema do projeto [Comunique-se!](http://comuniquese.ifrs.edu.br/) do [IFRS](http://www.ifrs.edu.br/).
Conta com um site público onde os usuários podem enviar informações sobre acontecimentos relevantes para os comunicadores das unidades da instituição. Além disso, possui um sistema simples para gerenciamento dos contatos (comunicadores) e das mensagens (informações) enviadas.


## Requisitos

- Servidor Apache com `mod_rewrite` habilitado.
- PHP 5.2.8 ou superior.
- `PHP mail` devidamente configurado com um servidor de emails.
- MySQL 5.x.


## Instalação

1. Copie todos os arquivos para uma pasta no servidor.
2. Dê permissão recursiva de escrita para as pastas `/app/Config`, `/app/Plugins/Install/Config`, `/app/tmp` e `/app/webroot/files`.
3. Crie uma base de dados vazia com a "collation" `ut8_general_ci` no seu servidor MySQL.
4. Acesse o endereço `http://[url_do_sistema]/install` e siga os passos do instalador.
5. Troque o arquivo `/app/webroot/img/logo.png` pelo logotipo da sua instituição, mantendo o mesmo nome (logo), extensão (png) e tipo (image/png).
6. Pronto, o sistema já deve estar funcional.

Para problemas no funcionamento do framework, você pode obter mais informações em: [http://book.cakephp.org/2.0/en/installation.html](http://book.cakephp.org/2.0/en/installation.html)


## Frameworks Utilizados

- [CakePHP](http://cakephp.org/) - sob a licença MIT.
- [JQuery](http://jquery.com/) - sob a licença MIT.
- [Twitter Bootstrap](http://getbootstrap.com/) - sob a licença MIT.
